<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/main.css">
    <title>Golden</title>
</head>

<body>
    <header>
        <div class="container">
            <div class="heading clearfix">
                <img src="img/logo.png" alt="Golden" class="logo">
                <nav>
                    <ul class="menu">
                        <li>
                             <a href="#">Home</a>
                        </li>
        
                        <li>
                             <a href="#">Services</a>
                        </li>
        
                        <li>
                            <a href="#">Portfolio</a>
                        </li>
                            
                        <li>
                             <a href="#">About</a>
                        </li>
                            
                        <li>
                             <a href="#">Contact</a>
                        </li>
                    </ul>
                </nav>
            </div>
            
            <div class="title">
                <div class="titles__first">
                    Welcome to our studio!
                </div>

                <h1>It's nice to Meet you</h1>
            </div>
            <a class="button" href="#">Tell Me More</a>
        </div>
    </header>


    <section>
        <div class="container" id="services">
            <div class="title">
                <h2>
                    Services
                </h2>

                <p>
                    Proin iaculis purus consequat sem cure.
                </p>
            </div>

            <div class="services clearfix">
                <div class="services__item">
                    <img src="img/icon1.png" alt="Услуга">
                    <h3>E-Commerce</h3>
                    <p>
                        Proin iaculis purus consequat sem cure 
                        digni ssim. Donec porttitora entum suscipit 
                        aenean rhoncus posuere odio in tincidunt.
                    </p>
                </div>

                <div class="services__item">
                    <img src="img/icon2.png" alt="Услуга">
                    <h3>Responsive Web</h3>
                    <p>
                        Proin iaculis purus consequat sem cure 
                        digni ssim. Donec porttitora entum suscipit 
                        aenean rhoncus posuere odio in tincidunt.
                    </p>
                </div>

                <div class="services__item">
                    <img src="img/icon3.png" alt="Услуга">
                    <h3>Web Security</h3>
                    <p>
                        Proin iaculis purus consequat sem cure 
                        digni ssim. Donec porttitora entum suscipit 
                        aenean rhoncus posuere odio in tincidunt.
                    </p>                    
                </div>
            </div>
        </div>
    </section>

    <section id="portfolio">
        <div class="container">
            <div class="title">
                <h2>
                    Our portfolio
                </h2>
        
                <p>
                    Proin iaculis purus consequat sem cure.                 </p>
            </div>
            <div class="works clearfix">
                <img src="img/img1.jpg" alt="Работа">
                <img src="img/img2.jpg" alt="Работа">
                <img src="img/img3.jpg" alt="Работа">
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="title">
                <h2>
                    About Us                
                </h2>
                
                <p>
                Proin iaculis purus consequat sem cure.
                </p>
            </div>

            <div class="aboutus_item_l clearfix">
                <div class="aboutus_text_l">
                    <h3 class="first_title">
                        July 2010
                    </h3>
                    <h3 class="second_title">
                        Our Humble Beginnings
                    </h3>
                    <p>
                        Proin iaculis purus consequat sem cure <br>
                        digni ssim. Donec porttitora entum suscipit <br>
                        aenean rhoncus posure odio in tinciunt. Proin <br>
                        iaculis purus consequat sem cure digni <br>
                        ssim. Donec porttitora entum suscipit.
                    </p>
                </div>
                <img src="img/about1.jpg" alt="О нас">
            </div>

            <div class="aboutus_item_r clearfix">
                <div class="aboutus_text_r">
                    <h3 class="first_title_r">
                          January 2011
                    </h3>
                    <h3 class="second_title_r"> 
                         Facing Startup Battles
                    </h3>
                    <p>
                        Proin iaculis purus consequat sem cure <br>
                        digni ssim. Donec porttitora entum suscipit <br>
                        aenean rhoncus posure odio in tinciunt. Proin <br>
                        iaculis purus consequat sem cure digni <br>                            ssim. Donec porttitora entum suscipit.
                    </p>
                </div>
                <img src="img/about2.jpg" alt="О нас">
            </div>

            <div class="aboutus_item_l clearfix">
                <div class="aboutus_text_l">
                    <h3 class="first_title">
                        December 2012                    
                    </h3>
                    <h3 class="second_title">
                        Enter The Dark Days
                    </h3>
                    <p>
                        Proin iaculis purus consequat sem cure <br>
                        digni ssim. Donec porttitora entum suscipit <br>
                        aenean rhoncus posure odio in tinciunt. Proin <br>
                        iaculis purus consequat sem cure digni <br>                            ssim. Donec porttitora entum suscipit.
                    </p>
                </div>
                <img src="img/about3.jpg" alt="О нас">
            </div>
    
            <div class="aboutus_item_r clearfix">
                <div class="aboutus_text_r">
                    <h3 class="first_title_r">
                        february 2014
                    </h3>
                    <h3 class="second_title_r"> 
                         Our Triumph
                    </h3>
                    <p>
                        Proin iaculis purus consequat sem cure <br>
                        digni ssim. Donec porttitora entum suscipit <br>
                        aenean rhoncus posure odio in tinciunt. Proin <br>
                        iaculis purus consequat sem cure digni <br>                            ssim. Donec porttitora entum suscipit.
                      </p>
                </div>
                <img src="img/about4.jpg" alt="О нас">
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <h6>&copy; Copyright 2019 Alexandr Alpatov</h6>
        </div>
    </footer>
</body>

</html>